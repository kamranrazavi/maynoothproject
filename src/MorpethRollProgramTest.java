import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class MorpethRollProgramTest {

    Person p = new Person("Kamran", "", "Razavi", "Ireland", "", 0);
    Person next = new Person("Adam", "", "Winstanley", "Ireland", "", 0);
    Person prev = new Person("Nafise", "", "Eskandani", "Ireland", "", 0);

    @Test
    public void writeResults() {
        String add = "data/test.csv";
        List<Person> personList = new ArrayList<>();
        p.fillPrevNext(prev, next);
        p.digit = "1";
        personList.add(p);
        MorpethRollProgram.writeResults(personList, add);
        File f = new File(add);
        assertNotNull(f);
    }

    @Test
    public void checkAllHavePrevNext() {
        List<Person> personList = new ArrayList<>();
        p.fillPrevNext(prev, next);
        p.digit = "1";
        personList.add(p);
        assertTrue(MorpethRollProgram.checkAllHavePrevNext(personList));
    }

    @Test
    public void checkAllHaveDigits() {
        List<Person> personList = new ArrayList<>();
        p.fillPrevNext(prev, next);
        p.digit = "1";
        personList.add(p);
        assertTrue(MorpethRollProgram.checkAllHaveDigits(personList));
    }

}