import java.util.ArrayList;

/**
 * This class represents a Person object from the roll and external resources.
 */
public class Person {

    final static double DEFAULT_CORR = 0.95; //Default correlation with another person. This number is chosen because after 50 iterators, its probability goes beneath 10%.

    String firstname;
    String middlename;
    String surname;
    String digit;
    String location;
    String fileName;
    double probLocation;
    int page;
    CorrelationWithAnotherPerson prev;
    CorrelationWithAnotherPerson next;

    ArrayList<CorrelationWithAnotherPerson> bestLocations;

    /**
     * Constructor.
     * @param f, Firstname.
     * @param m, Middlename.
     * @param s, Surname.
     * @param d, Digit.
     * @param l, Location.
     * @param p, Prev person.
     * @param n, Next person.
     * @param file, file's name.
     */
    public Person(String f, String m, String s, String d, String l, Person p, Person n, String file, int page) {
        bestLocations = new ArrayList<>();
        firstname = f;
        middlename = m;
        surname = s;
        digit = d;
        location = l;
        probLocation = 0;
        prev = new CorrelationWithAnotherPerson(p, p == null ? 0 : DEFAULT_CORR);
        next = new CorrelationWithAnotherPerson(n, n == null ? 0 : DEFAULT_CORR);
        fileName = file;
        this.page = page;
    }

    /**
     * Constructor.
     * @param f, Firstname.
     * @param m, Middlename.
     * @param s, Surname.
     * @param l, Location.
     * @param file, Files name.
     */
    public Person(String f, String m, String s, String l, String file, int page) {
        bestLocations = new ArrayList<>();
        firstname = f;
        middlename = m;
        surname = s;
        location = l;
        probLocation = 0;
        fileName = file;
        this.page = page;
    }

    /**
     * This function fills the next and prev of the current person.
     * @param p, Prev person.
     * @param n, Next Person.
     */
    public void fillPrevNext(Person p, Person n) {
        prev = new CorrelationWithAnotherPerson(p, p == null ? 0 : DEFAULT_CORR);
        next = new CorrelationWithAnotherPerson(n, n == null ? 0 : DEFAULT_CORR);
    }
}

/**
 * This class gives a person, a correlation with another one.
 */
class CorrelationWithAnotherPerson {
    Person person;
    double corr;

    /**
     * Constructor.
     * @param p, Person.
     * @param c, Correlation with another person.
     */
    public CorrelationWithAnotherPerson(Person p, Double c) {
        person = p;
        corr = c;
    }
}