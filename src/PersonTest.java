import static org.junit.Assert.*;

public class PersonTest {
    Person p = new Person("Kamran", "", "Razavi", "Ireland", "", 0);
    Person next = new Person("Adam", "", "Winstanley", "Ireland", "", 0);
    Person prev = new Person("Nafise", "", "Eskandani", "Ireland", "", 0);

    /**
     * This test checks that a prev and a next are assigned.
     */
    @org.junit.Test
    public void fillPrevNextNotNull() {
        p.fillPrevNext(prev, next);
        assertNotNull(p.next);
        assertNotNull(p.prev);
        assertEquals(p.next.person.surname.toLowerCase(), "winstanley");
        assertEquals(p.prev.person.firstname.toLowerCase(), "nafise");
    }

    /**
     * This test checks that a prev and a next are correctly assigned.
     */
    @org.junit.Test
    public void fillPrevNextCheckNames() {
        p.fillPrevNext(prev, next);
        assertNotNull(p.next);
        assertNotNull(p.prev);
        assertEquals(p.next.person.surname.toLowerCase(), "winstanley");
        assertEquals(p.prev.person.firstname.toLowerCase(), "nafise");
    }
}