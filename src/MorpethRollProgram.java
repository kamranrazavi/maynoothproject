import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 This class reads the roll and other resources, encodes and compare them, finds the best locations for all,
 and finally writes the results and encoded resources to different CSV files.
 */
public class MorpethRollProgram {

    private static final String STANDARD_SURNAMES = "data/SurnameIndex.xlsm"; //Standard Surnames
    private static final String STANDARD_FIRSTNAMES = "data/FirstNamesIndex.xlsm"; //Standard Firstnames
    private static final String ROLL_ADDRESS = "data/morpethroll.csv"; //Morpeth_Roll CSV address
    private static final String TRAINING_DATA_ADDRESS = "data/trainingdata/"; //The file containing all the training records
    private static final String ALL_TRAIN_DATA_CSV = "data/alltrain.csv"; //Writing all the recording data and their encoded formats in this CSV file
    private static final String LOCATIED_ROLL = "data/results.csv"; //Writing all the results in this CSV file

    private static HashMap<String, Integer> surnameNumbers; //Surnames to their encodes map
    private static HashMap<ShortName, Integer> firstnameNumbers; // Firstnames to their encodes map
    private static List<Person> morpethRoll; //Morpeth Roll data
    private static List<Person> trainingData; //All external resources

    /**
     * Main function, the program starts from here.
     * It runs the functions one by one.
     * @param args, default arguments, empty
     */
    public static void main(String[] args) {
        System.err.println("Filling the maps");
        fillMaps();
        System.err.println("Reading Morpeth Roll");
        readMorpethRoll();
        System.err.println("Reading Training Data");
        readTrainingData();
        System.err.println("Assigning Digit");
        assignDigits(morpethRoll);
        assignDigits(trainingData);
        System.err.println("Checking each data has a prev and a next except for two");
        checkAllHavePrevNext(morpethRoll);
        System.err.println("Checking all the data have digits");
        checkAllHaveDigits(morpethRoll);
        checkAllHaveDigits(trainingData);
        System.err.println("Matching Morpeth records to the Training data");
        findMatches();
        System.err.println("Finding nearest locations");
        bestLocationsForEachPerson();
        System.err.println("Writing the results to a CSV file");
        writeResults(morpethRoll, LOCATIED_ROLL);
        System.err.println("Writing the training data to a CSV file");
        writeResults(trainingData, ALL_TRAIN_DATA_CSV);
        System.err.println("Code is finished successfully");
    }

    /**
     * This function writes the results and training records into CSV files.
     * @param mr, Persons to be written on the CSV file
     * @param address, Address of the CSV file tha Persons should be written into it.
     */
    public static void writeResults(List<Person> mr, String address) {
        try {
            Writer writer = Files.newBufferedWriter(Paths.get(address));
            CSVWriter csvWriter = new CSVWriter(writer,
                    CSVWriter.DEFAULT_SEPARATOR,
                    CSVWriter.NO_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END);
            String[] headerRecord = {"Firstname", "Surname", "Page", "Digit","Location", "Probability", "Location 2", "Probability 2"};
            csvWriter.writeNext(headerRecord);
            for (Person p: mr)
                csvWriter.writeNext(new String[]{p.firstname, p.surname, p.page + "", p.digit, p.location, p.probLocation + "", p.bestLocations.size() > 1? p.bestLocations.get(1).person.location : "",
                        p.bestLocations.size() > 1? p.bestLocations.get(1).corr +"" : ""});
            csvWriter.flush();
            csvWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Finding locations of all Persons in the roll using Binary BFS.
     */
    public static void bestLocationsForEachPerson() {
        int foundLocations = 0;
        for (Person p: morpethRoll) {
            Map<String, Boolean> visited = new HashMap<>();
            LinkedList<Node> queue = new LinkedList<>();
            queue.add(new Node(p, 0));
            while (!queue.isEmpty()) {
                Node at = queue.pop();
                Boolean vis = visited.get(at.person.digit);
                if (vis != null)
                    continue;
                visited.put(at.person.digit, true);
                if (Math.pow(Person.DEFAULT_CORR, at.depth) < 0.10)
                    continue;
                if (at.person.location.length() != 0) {
                    p.bestLocations.add(new CorrelationWithAnotherPerson(at.person, Math.pow(Person.DEFAULT_CORR, at.depth)));
                    if (p.bestLocations.size() == 5)
                        break;
                }
                if (at.person.next.person != null)
                    queue.add(new Node(at.person.next.person, at.depth + 1));
                if (at.person.prev.person != null)
                    queue.add(new Node(at.person.prev.person, at.depth + 1));
            }
            if (p.bestLocations.size() != 0) {
                foundLocations++;
            }
        }
        System.out.println("Number of located persons in the roll with the probabilities of > 10%) " + foundLocations + " out of " + morpethRoll.size());
        for (Person p: morpethRoll)
            if (p.bestLocations.size() > 0) {
                p.location = p.bestLocations.get(0).person.location;
                p.probLocation = p.bestLocations.get(0).corr;
            }
    }

    /**
     * This function matches encodes from training records to the roll records
     */
    public static void findMatches() {
        int counter = 0;
        for (Person m: morpethRoll)
            for (Person t: trainingData) {
                t.probLocation = 1;
                if (m.digit.equals(t.digit)) {
                    m.location = t.location;
                    m.probLocation = 1;
                    counter++;
                    break;
                }
            }
        System.out.println("Number of matched names based on their digits: " + counter);
    }

    /**
     * This function checks all the records in the roll has a prev and a next.
     * @param morpethRoll, the roll's records
     */
    public static boolean checkAllHavePrevNext(List<Person> morpethRoll) {
        int count = 0;
        for (Person p: morpethRoll) {
            if (p.prev.person == null) {
                count++;
            }
            if (p.next.person == null) {
                count++;
            }
        }
        if (count > 2) {
            System.out.println("Error! More than two persons have missing next/prev");
            System.exit(0);
        }
        return true;
    }

    /**
     * This function checks all the persons in the list are assigned a digit.
     * @param persons, the list that should be checked.
     */
    public static boolean checkAllHaveDigits(List<Person> persons) {
        for (Person p: persons)
            if (p.digit == null) {
                System.out.println(p.firstname + " " + p.surname);
                return false;
            }
        return true;
    }

    /**
     * This function assigns digits to the list.
     * @param persons, the list that a digit should be assigned.
     */
    public static void assignDigits(List<Person> persons) {
        int firstnameLastNumberInMap = -1;
        int surnameLastNumberInMap = -1;

        for (Integer num: firstnameNumbers.values())
            if (firstnameLastNumberInMap < num)
                firstnameLastNumberInMap = num + 1;
        for (Integer num: surnameNumbers.values())
            if (surnameLastNumberInMap < num)
                surnameLastNumberInMap = num + 1;

        for (Person p: persons) {
            String number = "";
            boolean found = false;
            if (p.firstname != null) {
                for (ShortName sn : firstnameNumbers.keySet()) {
                    if (sn.shortName.equals(p.firstname) || sn.standardName.equals(p.firstname)) {
                        found = true;
                        number = (firstnameNumbers.get(sn)) + "";
                        break;
                    }
                }
                if (!found) {
                    number = (firstnameLastNumberInMap) + "";
                    firstnameNumbers.put(new ShortName(p.firstname, p.firstname), firstnameLastNumberInMap++);
                }
            }
            p.digit = number;
        }

        for (Person p: persons) {
            String number = "";
            Integer res = surnameNumbers.get(p.surname);
            if (res == null) {
                number = surnameLastNumberInMap + "";
                surnameNumbers.put(p.surname, surnameLastNumberInMap++);
            } else {
                number = res + "";
            }
            while (number.length() < 5)
                number = "0" + number;
            p.digit += number;
        }

    }

    /**
     * This function reads all the available resources and puts all of them in a list.
     */
    public static void readTrainingData() {
        File[] files = new File(TRAINING_DATA_ADDRESS).listFiles();
        trainingData = new ArrayList<>();
        for (int i = 0; i < files.length; i++) {
            try {
                Workbook workbook = WorkbookFactory.create(files[i]);
                Sheet sheet = workbook.getSheetAt(0);
                int firstnameIndex = -1, middlenameIndex = -1, surnameIndex = -1, locationIndex = -1;
                for (int j = 1; j <= sheet.getRow(0).getLastCellNum(); j++) {
                    Cell currentCell = sheet.getRow(0).getCell(j);
                    if (currentCell == null)
                        continue;
                    String cellName = currentCell.getRichStringCellValue().toString();
                    if (firstnameIndex == -1 && (cellName.toLowerCase().contains("firstname") || cellName.toLowerCase().contains("forename"))) {
                        firstnameIndex = j;
                    } else if (middlenameIndex == -1 && cellName.toLowerCase().contains("middlename")) {
                        middlenameIndex = j;
                    } else if (surnameIndex == -1 && cellName.toLowerCase().contains("surname")) {
                        surnameIndex = j;
                    } else if (cellName.toLowerCase().contains("townland")) {
                        locationIndex = j;
                    }
                }
                for (int j = 1; j < sheet.getLastRowNum(); j++) {
                    if (sheet.getRow(j) == null)
                        continue;
                    Cell firstname = null;
                    if(firstnameIndex != -1)
                        firstname = sheet.getRow(j).getCell(firstnameIndex);
                    Cell middlename = null;
                    if (middlenameIndex != -1)
                        middlename = sheet.getRow(j).getCell(middlenameIndex);
                    Cell surname = null;
                    if (surnameIndex != -1)
                        surname = sheet.getRow(j).getCell(surnameIndex);
                    Cell location = null;
                    if (locationIndex != -1)
                        location = sheet.getRow(j).getCell(locationIndex);
                    if ((firstname == null || firstname.getRichStringCellValue().getString().length() == 0) && (surname == null || surname.getRichStringCellValue().getString().length() == 0))
                        continue;
                    if (location == null || location.getRichStringCellValue().getString().length() == 0)
                        continue;
                    trainingData.add(new Person(firstname == null ? null : firstname.getRichStringCellValue().getString().toLowerCase(),
                            middlename == null ? null : middlename.getRichStringCellValue().getString().toLowerCase(),
                            surname == null ? null : surname.getRichStringCellValue().getString().toLowerCase(),
                            location.getRichStringCellValue().getString().toLowerCase().replace(" ", ""), files[i].getName(), 0));
                }
            } catch (InvalidFormatException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * This function reads the roll and puts it in a list.
     */
    public static void readMorpethRoll() {
        try {
            CSVReader reader = new CSVReader(new FileReader(ROLL_ADDRESS), ',');
            morpethRoll = new ArrayList<>();
            List<String[]> records = reader.readAll();
            for (int i = 1; i < records.size(); i++) {
                String[] currentRow = records.get(i);
                if (currentRow[0] == null || currentRow[0].length() == 0)
                    continue;
                String f = (currentRow[3] == null || currentRow[3].length() == 0) ? currentRow[4].toLowerCase() : currentRow[3].toLowerCase();
                String m = currentRow[5].toLowerCase();
                String s = currentRow[6].toLowerCase();
                int page = Integer.parseInt(currentRow[0]);
                morpethRoll.add(new Person(f, m, s, "", "morpethroll.csv", page));
            }

            for (int i = 0; i < morpethRoll.size(); i++) {
                Person prev = null, next = null;
                if (i > 0)
                    prev = morpethRoll.get(i - 1);
                if (i < morpethRoll.size() - 1)
                    next = morpethRoll.get(i + 1);
                morpethRoll.get(i).fillPrevNext(prev, next);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This function reads the Firstnames and Surnames and their assigned digits.
     */
    public static void fillMaps() {
        try {
            surnameNumbers = new HashMap<>();
            Workbook workbook = WorkbookFactory.create(new File(STANDARD_SURNAMES));
            Sheet sheet = workbook.getSheetAt(0);
            for (int i = 1; i < sheet.getLastRowNum(); i++) {
                Cell cellName = sheet.getRow(i).getCell(0), cellNumber = sheet.getRow(i).getCell(1);
                surnameNumbers.put(cellName.getRichStringCellValue().getString().toLowerCase(), Integer.parseInt((cellNumber.getNumericCellValue() + "").substring(0, (cellNumber.getNumericCellValue() + "").length() - 2)));
            }

            firstnameNumbers = new HashMap<>();
            workbook = WorkbookFactory.create(new File(STANDARD_FIRSTNAMES));
            sheet = workbook.getSheetAt(0);
            for (int i = 1; i < sheet.getLastRowNum(); i++) {
                Cell cellShortName = sheet.getRow(i).getCell(0), cellStandardName = sheet.getRow(i).getCell(1), cellNumber = sheet.getRow(i).getCell(3);
                if (cellStandardName == null)
                    cellStandardName = cellShortName;
                firstnameNumbers.put(new ShortName(cellShortName.getRichStringCellValue().getString().toLowerCase(), cellStandardName.getRichStringCellValue().getString().toLowerCase()), Integer.parseInt((cellNumber.getNumericCellValue() + "").substring(0, (cellNumber.getNumericCellValue() + "").length() - 2)));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }

    }
}

/**
 * This class stores the short name and standard name of each person in the Firstname data.
 */
class ShortName {
    String shortName, standardName;

    /**
     * Constructor.
     * @param a, short name.
     * @param b, standard name.
     */
    public ShortName(String a, String b) {
        shortName = a;
        standardName = b;
    }
}

/**
 * This class represents a node in the Binary BFS algorithm.
 */
class Node {
    Person person;
    int depth;

    /**
     * Constructor.
     * @param p, Person
     * @param d, Depth that this person has been seen in the BBFS.
     */
    public Node(Person p, int d) {
        person = p;
        depth = d;
    }
}